import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;


export class GlobalModel {
    private static globalMode: GlobalModel = null;
    private constructor() { }

    public static getInstances(): GlobalModel {
        if (!this.globalMode) {
            this.globalMode = new GlobalModel();
        }
        return this.globalMode;
    }
    /**
     * 关卡配置信息
     */
    private levelConfig: any = null;
    /**
     * 设置关卡配置信息
     */
    public setLevelConfig(data: any): void {
        this.levelConfig = data;
    }
    /**
     * 获取关卡配置信息
     */
    public getLevelConfig(): any {
        return this.levelConfig;
    }
    /**
     * 获取指定关卡配置信息
     */
    public getLevelConfigByKey(key: string): any {
        return this.levelConfig[key];
    }
    /**
     * 设置选择模式
     */
    private selectMode: number = 0
    private selectLevel: number = 0
    // private selectMusicKey:string="";
    private selectMusicData: string = "";
    public setSelectMode(num: number): void {
        this.selectMode = num;
    }
    /**
     * 设置当前模式的选择关卡
     */
    public setSelectlevel(num: number): void {
        this.selectLevel = num;
        let key: string = this.selectMode + "_" + this.selectLevel;
        console.log(key)
        this.selectMusicData = this.getLevelConfigByKey(key);
    }
    public getSelectMode(): number {
        return this.selectMode;
    }
    public getSelectlevel(): number {
        return this.selectLevel;
    }
    /**
     * 获取当前选中模式歌曲的数量
     */
    public getSelectModeNum():number{
        if(this.selectMode==0){
            return this.levelConfig["config"]["cartoon"];
        }else if(this.selectMode==1){
            return this.levelConfig["config"]["fashion"];
        }else if(this.selectMode==2){
            return this.levelConfig["config"]["class"];
        }
    }
    

    /**
     * 
     */
    public getRhythmPointData(): Array<number> {
        return JSON.parse(this.selectMusicData["rhythm"]);
    }
    /**
     * 获取当前关键音乐键值
     */
    public getMisicKeyLevel(): string {
        let key: string = this.selectMode + "_" + this.selectLevel;
        return key;
    }
    /**
     * 获取当前歌曲名称
     */
    public getMisicNameLevel(): string {
        return this.selectMusicData["name"];
    }
    /**
     * 获取当前模块和关卡的数据点
     */
    public getAmpPointData(): Array<number> {
        return JSON.parse(this.selectMusicData["amp"]);
    }
    /**
     * 获取歌曲的时间
     */
    public getMisicTimerLevel(): number {
        return Math.floor(this.selectMusicData["timer"]);
    }
    /**
     * 选择的关卡难度等级
     */
    private difficultGrade: number = 0;
    public setDifficultGrade(num: number): void {
        this.difficultGrade = num;
    }
    public getDifficultGrade(): number {
        return this.difficultGrade;
    }

}
