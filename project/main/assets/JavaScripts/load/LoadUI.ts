import { _decorator, Component, Node, director, loader, JsonAsset } from 'cc';
import { GlobalModel } from '../global/GlobalModel';
const { ccclass, property } = _decorator;

@ccclass('LoadUI')
export class LoadUI extends Component {

    start() {
        if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            this.loadSubpackage();
        } else {
            this.preLoadScene();
        }
    }

    /**
    * 加载分包资源
    */
    private loadSubpackage(): void {
        // let self = this;
        // let wx = window["wx"];
        // let loadTaskm2d = wx.loadSubpackage({
        //     name: "2d",
        //     success: function (res) {
        //         let loadTaskm3d = wx.loadSubpackage({
        //             name: "3d",
        //             success: function (res) {
        //                 self.preLoadScene();
        //             },
        //             fail: function (res) {
        //             },
        //             complete(res) { }
        //         });
        //         loadTaskm3d.onProgressUpdate((res) => {
        //             let progress: number = Math.floor(res.progress) > 100 ? 100 : Math.floor(res.progress);
        //             self.refreshLoadProgress(Math.floor(res.progress), "3d模型加载中..." + Math.floor(progress) + "%");
        //         });
        //     },
        //     fail: function (res) {
        //     },
        //     complete(res) {
        //     }
        // });
        // loadTaskm2d.onProgressUpdate((res) => {
        //     let progress: number = Math.floor(res.progress) > 100 ? 100 : Math.floor(res.progress);
        //     self.refreshLoadProgress(Math.floor(res.progress), "2d资源加载中..." + progress + "%");
        // });
        this.preLoadScene();
    }
    /**
     * 预加载场景
     */
    private preLoadScene(): void {
        // // SoundController.getInstances().init();
        // SDKPlatformController.init();
        // // DialogController.getInstantes().init();
        // // PlayerControllerImpl.getInstances().init();
        // director.preloadScene("start",
        //     (completedCount: number, totalCount: number, item: any) => {
        //         // console.log("资源加载中..." + Math.floor(completedCount / totalCount * 100) + "%");
        //         this.refreshLoadProgress(Math.floor(completedCount / totalCount * 100), "场景初始化中..." + Math.floor(completedCount / totalCount * 100) + "%");
        //     }, (error, sceneAsset) => {
        //         if (error) {
        //             console.log(error)
        //             return;
        //         }
        //         director.preloadScene("game",
        //             (completedCount: number, totalCount: number, item: any) => {
        //                 this.refreshLoadProgress(Math.floor(completedCount / totalCount * 100), "场景初始化中..." + Math.floor(completedCount / totalCount * 100) + "%");
        //             }, (error, sceneAsset) => {
        //                 if (error) {
        //                     console.log(error)
        //                     return;
        //                 }
        //                 this.initGame();
        //             }
        //         );
        //     }
        // );
        this.initGame();
    }
    /**
     * 初始化
     */
    private initGame(): void {
        this.initPool();
    }
    /**
     * 初始化对象池
     */
    private initPool(): void {
        let self = this;
        let allNum: number = 1;
        let indexNum: number = 0;
        loader.loadRes("levelConfig/level", JsonAsset, (err, jsonRes) => {
            if (err) return;
            GlobalModel.getInstances().setLevelConfig(jsonRes.json);
            indexNum++;
            self.checkPoolState(indexNum, allNum)
        });


        // let self = this;

    }
    private checkPoolState(num: number, allNum: number): void {
        this.refreshLoadProgress(Math.floor(num / allNum * 100), "资源初始化中..." + Math.floor(num / allNum * 100) + "%");
        if (num >= allNum) {
            director.loadScene("start");
        }
    }
    /**
     * 刷新加载进度
     * progress  百分比
     */
    private refreshLoadProgress(progress: number, msg: string): void {
        // this.labelLoad.getComponent(LabelComponent).string = msg;
        // this.loadBar.width = 346 / 100 * progress;
        // if (this.loadBar.width > 346) {
        //     this.loadBar.width = 346;
        // }
    }
    update(deltaTime: number) {

    }
}
