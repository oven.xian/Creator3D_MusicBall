import { _decorator, Component, Node, AudioClip, loader, Vec2, Vec3, instantiate, UIModelComponent, UITransformComponent, AudioSourceComponent, LabelComponent, director } from 'cc';
import { GlobalModel } from '../global/GlobalModel';
import { IStartUI } from './StartContract';
import { StartModel } from './StartModel';
const { ccclass, property } = _decorator;

@ccclass('StartUI')
export class StartUI extends Component implements IStartUI {
    
    @property({type:LabelComponent})
    labelMusicName:LabelComponent=null;
    @property({type:Node})
    btnLastMusic:Node=null;
    @property({type:Node})
    btnNextMusic:Node=null;

    private mStartMode: StartModel = null;
    private audioSource: AudioSourceComponent = null;
    /**
     * 是否可以切换音乐
     */
    private isCutMusic:boolean=false;   
    start() {
        this.init();
    }
    //
    init(): void {
        this.initData();
        this.initUI();
        this.initEvent();
        this.playSelectMusic();
    }
    //
    initData(): void {
        this.audioSource = this.node.getComponent(AudioSourceComponent);
        this.audioSource.loop = true;
        this.mStartMode = new StartModel();
        GlobalModel.getInstances().setSelectMode(1);
        let selectLevelID: number = this.mStartMode.getSelectLevel();
        GlobalModel.getInstances().setSelectlevel(selectLevelID);
        this.isCutMusic=true;
    }
    //
    initUI(): void {
        this.labelMusicName.string=GlobalModel.getInstances().getMisicNameLevel();
        if(GlobalModel.getInstances().getSelectlevel()<=0){
            this.btnLastMusic.active=false;
        }else{
            this.btnLastMusic.active=true;
        }
        if(GlobalModel.getInstances().getSelectlevel()>=GlobalModel.getInstances().getSelectModeNum()-1){
            this.btnNextMusic.active=false;
        }else{
            this.btnNextMusic.active=true;
        }

    }
    //
    initEvent():void{

    }
    //
    playSelectMusic(): void {
        let self = this;
        this.isCutMusic=false;
        this.audioSource.stop();
        loader.loadRes("music/main/" + GlobalModel.getInstances().getMisicKeyLevel(), AudioClip, (err, audioClip) => {
            if(err) {console.log(err) ;return;}
            self.audioSource.clip= audioClip;
            self.audioSource.play();
            self.isCutMusic=true;
        });
    }
    btnStartGameEvent(): void {
        this.audioSource.stop();
        director.loadScene("game");
    }
    btnLastMusicEvent(): void {
        if(!this.isCutMusic) return;
        let nowLevelID: number = GlobalModel.getInstances().getSelectlevel();
        if(nowLevelID<=0) return;
        nowLevelID--;
        GlobalModel.getInstances().setSelectlevel(nowLevelID);
        this.initUI();
        this.playSelectMusic();
    }
    btnNextMusicEvent(): void {
        if(!this.isCutMusic) return;
        let nowLevelID: number = GlobalModel.getInstances().getSelectlevel();
        if(nowLevelID>=GlobalModel.getInstances().getSelectModeNum()-1) return;
        nowLevelID++;
        GlobalModel.getInstances().setSelectlevel(nowLevelID);
        this.initUI();
        this.playSelectMusic();
    }


    update(deltaTime: number) {

    }
}



// @property({ type: Node })
// cadencePanel: Node = null;

// @property({ type: Node })
// line: Node = null;

// private musicCadenceIndex: number = 0;
// private numicOffset: number = 0;
// private refreshCadenceSphere: boolean = true;
// music: AudioClip = null;
// ampPointDatas: Array<number> = null;
// start() {
//     this.initLoadMusic();
// }




// private initLoadMusic(): void {
//     this.musicCadenceIndex = 0;
//     this.refreshCadenceSphere = false;
//     let self = this;
//     loader.loadRes("music/main/" + GlobalModel.getInstances().getMisicNameLevel(), AudioClip, (err, audioClip) => {
//         self.startPlayMusic(audioClip)
//     });
// }
// private startPlayMusic(music: AudioClip): void {
//     this.ampPointDatas = GlobalModel.getInstances().getAmpPointData();
//     let timer: number = GlobalModel.getInstances().getMisicTimerLevel();
//     this.numicOffset = Math.floor(this.ampPointDatas.length / timer/ 2) ;
//     // console.log(this.numicOffset);
//     this.music = music;

//     let lineNum: number = this.numicOffset;
//     for (let i = 0; i < lineNum; i++) {
//         let item = instantiate(this.line);
//         this.cadencePanel.addChild(item);
//         let x: number = (i - lineNum / 2) / 2 * 50;
//         let y: number = -120;
//         item.position = new Vec3(x, y, 0);
//         item.getComponent(UITransformComponent).height=0;
//     }

//     this.refreshCadenceSphere = true;
//     this.music.play();
//     this.playRhythmAnim();
//     this.schedule(this.playRhythmAnim,0.5);
// }
// private playRhythmAnim(): void {
//     let j=0;
//     for (let i = this.musicCadenceIndex; i < this.musicCadenceIndex + this.numicOffset; i++) {
//         let hScale: number = this.ampPointDatas[i]*2;
//         let line: Node = this.cadencePanel.children[j];
//         // line.scale=new Vec3(1,hScale,1);
//         let height:number=240*hScale>240?240:240*hScale;
//         line.getComponent(UITransformComponent).height=height;
//         j++
//     }
//     this.musicCadenceIndex += this.numicOffset;
//     if(this.musicCadenceIndex+this.numicOffset<this.ampPointDatas.length){

//     }else{
//         this.unschedule(this.playRhythmAnim);
//         this.music.stop();
//     }

// }




// update(deltaTime: number) {
//     // if (this.refreshCadenceSphere) {
//     //     let scale = this.ampPointDatas[this.musicCadenceIndex]
//     //     if(scale<0.1) scale=0.1;
//     //     this.cadenceSphere.scale = new Vec3(scale, scale, scale);
//     //     this.musicCadenceIndex++;
//     // }
// }